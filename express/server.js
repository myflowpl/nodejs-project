const {expressApp} = require('./app');

expressApp.listen(3000, () => console.log(`Example app listening on port 3000!`));
