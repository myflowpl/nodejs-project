import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UserRegisterRequestDto, UserRegisterResponseDto, UserLoginRequestDto, UserLoginResponseDto } from '../src/user/dto';
import { TokenPayloadModel, UserModel, UserRole } from '../src/models';
import { ConfigService } from '../src/config/config.service';
import { readFileSync } from 'fs';
import { resolve } from 'path';

describe('UserController (e2e)', () => {
  let app;
  let config: ConfigService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
    // .overrideProvider()
    // .useClass()
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    config = app.get(ConfigService);
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/user')
      .expect(403);
  });

  it('/user/register (POST)', () => {

    const req: UserRegisterRequestDto = {
      name: 'piotr',
      email: 'piotr@myflow.pl',
      password: '123',
    };

    const res: UserRegisterResponseDto = {
      user: {
        id: 2,
        name: 'piotr',
        email: 'piotr@myflow.pl',
        password: '123',
        roles: [],
      },
    };

    return request(app.getHttpServer())
      .post('/user/register')
      .send(req)
      .expect(201)
      .expect(res);
  });

  let token: string;

  it('/user/login SUCCESS', () => {

    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '123',
    };

    const resBody: UserLoginResponseDto = {
      token: expect.any(String),
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      },
    };
    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(201)
      .then(res => {
        token = res.body.token;
        expect(res.body).toMatchObject(resBody);
      });
  });

  it('/user/login ERROR', () => {

    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '0004',
    };

    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(404);
  });

  it('/user (GET)', () => {
    const resBody: UserModel = {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
        roles: [UserRole.ADMIN],
    };
    // const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJuYW1lIjoicGlvdHIiLCJlbWFpbCI6InBpb3RyQG15Zmxvdy5wbCJ9LCJpYXQiOjE1NDQ3OTc2NTl9.j3eoiUqVD1IbfFY6YfA_GzEnCfEcVxBw_wTV9jXCX-M';
    return request(app.getHttpServer())
      .get('/user')
      .set(config.TOKEN_HEADER_NAME, token)
      .expect(200)
      .then(res => {
        expect(res.body).toMatchObject(resBody);
      });
  });

  it('/user (GET) AuthGuard Forbidden', () => {
    return request(app.getHttpServer())
      .get('/user')
      .expect(403);
  });

  /**
   * upload photo
   */
  it('should upload photo', () => {

    const fileName = 'logo.png';

    const resBody = {
      avatar: {
        fileName: expect.any(String),
      },
      body: {
        myFile: 'my-file-name.png',
      },
      file: expect.any(Object),
    };

    return request(app.getHttpServer())

      .post('/photos/upload-user-avatar')

      .field('myFile', 'my-file-name.png')

      .attach('file', readFileSync(resolve('./src/fixtures/' + fileName)), fileName)

      .then(res => {
        expect(res.body).toMatchObject(resBody);
        expect(res).toHaveProperty('statusCode', 201);
      });
  });
});
