import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export enum UserRole {
  ADMIN = 'admin',
  ROOT = 'root',
}

export class UserModel {
  @ApiModelPropertyOptional()
  id?: number;
  @ApiModelProperty()
  name: string;
  @ApiModelPropertyOptional()
  email?: string;
  @ApiModelPropertyOptional()
  password?: string;

  @ApiModelPropertyOptional({enum: UserRole})
  roles?: UserRole[];
}

// export class UserModel {
//   id?: number;
//   name: string;
//   email?: string;
//   password?: string;
//   roles?: UserRole[];
//   // getName(): string;
// }

// TODO
// const user: UserModel = {
//   id: 4,
//   name: 'piotr',
// };
// console.log(user);

export class TokenPayloadModel {
  user: UserModel;
}
