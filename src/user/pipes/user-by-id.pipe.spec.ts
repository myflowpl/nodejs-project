import { UserByIdPipe } from './user-by-id.pipe';
import { NotFoundException } from '@nestjs/common';
import { UserService } from '../services';
import { UserModel } from '../../models';

describe('UserByIdPipe', () => {

  const user: UserModel = {
    id: 1,
    name: 'Piotr',
  };

  const userServiceMock: Partial<UserService> = {
    async getById(id: number) {
      if (id !== 1) {
        console.log('id', id);
        return null;
      }
      return user;
    },
  };
  let pipe: UserByIdPipe;

  beforeAll(() => {
    pipe = new UserByIdPipe(userServiceMock as UserService);
  });

  it('should return user model', () => {

    expect(pipe.transform('1')).resolves.toMatchObject(user);

  });

  it('should throw NotFoundException', () => {

    expect(pipe.transform('2')).rejects.toThrow(NotFoundException);

  });
  
});
