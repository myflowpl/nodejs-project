import { Controller, Post, Body, Request, Get, UseGuards, HttpException, HttpStatus, UsePipes, ValidationPipe, Param, ParseIntPipe } from '@nestjs/common';
import { UserService, AuthService } from '../services';
import { UserRegisterRequestDto, UserRegisterResponseDto, UserLoginRequestDto, UserLoginResponseDto } from '../dto';
import { ApiConsumes, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { User } from '../decorators/user.decorator';
import { UserModel, UserRole } from '../../models';
import { Roles } from '../decorators/roles.decorator';
import { AuthGuard } from '../guards/auth.guard';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';

@Controller('user')
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) { }

  @UsePipes(new ValidationPipe({ transform: true }))
  @Post('login')
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('NotFound', HttpStatus.NOT_FOUND);
    }
    return {
      token: this.authService.tokenSign({ user }),
      user,
    };
  }

  @Get()
  @UseGuards(AuthGuard)
  // @Roles({
  //   roles: [UserRole.ROOT],
  //   permissions: [],
  // })
  @Roles(UserRole.ADMIN)
  @ApiBearerAuth()
  getUser(@User() user: UserModel) {
    return user;
  }

  @Post('register')
  @ApiConsumes()
  @ApiResponse({ type: UserRegisterResponseDto, status: 201 })
  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {

    const user = await this.userService.create(data);

    // TODO handle errors
    return {
      user,
    };
  }

  @Get(':id')
  // @UsePipes(ParseIntPipe)
  getUserById(@Param('id', UserByIdPipe) user: UserModel) {

    return {
      user,
    };
  }
}
