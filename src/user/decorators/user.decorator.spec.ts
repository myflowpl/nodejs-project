import { User } from './user.decorator';
import * as express from 'express';

describe('User Decorator', () => {
  const req: Partial<express.Request> = {
    tokenPayload: {
      user: {
        id: 1,
        name: 'Piotr',
      },
    },
  };

  it('should return user from request', () => {

    // tslint:disable-next-line
    expect(User['factory']({}, req as express.Request)).toMatchObject(req.tokenPayload.user);

  });

  it('should return undefined request', () => {

    // tslint:disable-next-line
    expect(User['factory']({}, {} as express.Request)).toBeUndefined();
  });
});
