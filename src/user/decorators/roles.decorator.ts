import { ReflectMetadata } from '@nestjs/common';

export interface RolesOptions {
  /**
   * @description RolesModel
   */
  roles: string[];
  permissions: string[];
}
export const Roles = (...args: string[]) => ReflectMetadata('roles', args);
