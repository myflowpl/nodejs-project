import { createParamDecorator } from '@nestjs/common';
import * as express from 'express';
import { TokenPayloadModel } from '../../models';

declare module 'express' {
  export interface Request {
     tokenPayload?: TokenPayloadModel;
  }
}

function userDecoratorFactory(data: any, req: express.Request) {
  return (req.tokenPayload && req.tokenPayload.user) ? req.tokenPayload.user : undefined;
}

export const User = createParamDecorator(userDecoratorFactory);

// tslint:disable-next-line
User['factory'] = userDecoratorFactory;
