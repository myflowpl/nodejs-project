import { UserModel } from '../../models';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsString, MinLength, IsEmail } from 'class-validator';

export class UserRegisterRequestDto {
  @ApiModelProperty()
  name: string;

  @ApiModelProperty({
    description: 'email wymagany z domeny custom.com',
    example: 'piotr@myflow.pl',
  })
  email: string;

  @ApiModelPropertyOptional()
  password: string;
}

export class UserRegisterResponseDto {
  @ApiModelProperty()
  user: UserModel;
}

export class UserLoginRequestDto {

  @IsEmail()
  @ApiModelProperty({example: 'piotr@myflow.pl'})
  email: string;

  @IsString()
  @MinLength(3)
  @ApiModelProperty({example: '123'})
  password: string;

}

export class UserLoginResponseDto {
  token: string;
  user: UserModel;
}
