import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { ConfigModule } from '../../config/config.module';
import { ConfigService } from '../../config/config.service';
import { AppModule } from '../../app.module';
import { UserModule } from '../user.module';
import { TokenPayloadModel } from '../../models';

describe('AuthService', () => {
  let service: AuthService;

  // beforeAll(function() {

  // });
  // afterAll(function() {

  // });

  beforeEach(async () => {
    const config: Partial<ConfigService> = {
      JWT_SECRET_REFACTOR: 'jwt-secret',
    };

    // service = new AuthService(config as ConfigService);

    const module: TestingModule = await Test.createTestingModule({
      // imports: [ConfigModule],
      // providers: [AuthService, {
      //   provide: ConfigService,
      //   useValue: config,
      // }],
      imports: [AppModule],
    })
      // .overrideProvider(ConfigService)
      // .useValue(config)
      .compile();

    service = module.select(UserModule).get<AuthService>(AuthService);
  });

  it('should be defined', () => {

    expect(service).toBeDefined();

    // expect(mailGatwayMock.messages.length).toEqual(3);
  });

  it('token generation', () => {

    const payload: TokenPayloadModel = {
        user: {
          id: 1,
          name: 'piotr',
          email: 'piotr@myflow.pl',
        },
      };

    const token = service.tokenSign(payload);

    expect(typeof token).toBe('string');

    const decodedToken = service.tokenDecode(token);

    expect(decodedToken).toMatchObject(payload);

  });

});
