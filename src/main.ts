import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import * as express from 'express';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import { ConfigService } from './config/config.service';

// import {expressApp} from '../express/app';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');

  app.use(express.static(join(__dirname, 'assets/rxjs-project')));

  const config = new ConfigService();

  app.use('/assets/photos', express.static(config.STORAGE_PHOTOS));

  const options = new DocumentBuilder()
    .setTitle('Nest API Example')
    .setDescription('Przykładowy projekt w Node.js i TypeScript')
    .setVersion('1.0')
    .setBasePath('api')
    .addTag('user')
    .addBearerAuth(config.TOKEN_HEADER_NAME, 'header')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();

export interface Config {
  getConfig(env: string): Config;
}
