import { Controller, Post, UseInterceptors, FileInterceptor, Body, UploadedFile } from '@nestjs/common';
import { ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { PhotosService } from '../services/photos.service';

@Controller('photos')
export class PhotosController {

  constructor(private photosService: PhotosService) {}

  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Upload user avatar' })

  @Post('upload-user-avatar')

  @UseInterceptors(FileInterceptor('file'))
  
  async uploadFile(@UploadedFile() file, @Body() body) {

    const avatar = await this.photosService.create(file);
    
    return {avatar, file, body};
  }
}
