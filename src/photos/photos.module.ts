import { Module, MulterModule } from '@nestjs/common';
import { PhotosController } from './controllers/photos.controller';
import { PhotosService } from './services/photos.service';
import { ConfigService } from '../config/config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as entities from './entities';

@Module({
  imports: [
    TypeOrmModule.forFeature(Object.values(entities)),
    MulterModule.registerAsync({
      useFactory: async (config: ConfigService) => ({
        dest: config.STORAGE_TMP,
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [PhotosController],
  providers: [PhotosService],
})
export class PhotosModule {}
