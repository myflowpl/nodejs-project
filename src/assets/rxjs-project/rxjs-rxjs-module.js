(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rxjs-rxjs-module"],{

/***/ "./node_modules/rxjs/_esm5/ajax/index.js":
/*!***********************************************!*\
  !*** ./node_modules/rxjs/_esm5/ajax/index.js ***!
  \***********************************************/
/*! exports provided: ajax, AjaxResponse, AjaxError, AjaxTimeoutError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _internal_observable_dom_ajax__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../internal/observable/dom/ajax */ "./node_modules/rxjs/_esm5/internal/observable/dom/ajax.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return _internal_observable_dom_ajax__WEBPACK_IMPORTED_MODULE_0__["ajax"]; });

/* harmony import */ var _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../internal/observable/dom/AjaxObservable */ "./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxResponse", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_1__["AjaxResponse"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxError", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_1__["AjaxError"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxTimeoutError", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_1__["AjaxTimeoutError"]; });

/** PURE_IMPORTS_START  PURE_IMPORTS_END */


//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js":
/*!***************************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js ***!
  \***************************************************************************/
/*! exports provided: ajaxGet, ajaxPost, ajaxDelete, ajaxPut, ajaxPatch, ajaxGetJSON, AjaxObservable, AjaxSubscriber, AjaxResponse, AjaxError, AjaxTimeoutError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxGet", function() { return ajaxGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxPost", function() { return ajaxPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxDelete", function() { return ajaxDelete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxPut", function() { return ajaxPut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxPatch", function() { return ajaxPatch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxGetJSON", function() { return ajaxGetJSON; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxObservable", function() { return AjaxObservable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxSubscriber", function() { return AjaxSubscriber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxResponse", function() { return AjaxResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxError", function() { return AjaxError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxTimeoutError", function() { return AjaxTimeoutError; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _util_root__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../util/root */ "./node_modules/rxjs/_esm5/internal/util/root.js");
/* harmony import */ var _util_tryCatch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../util/tryCatch */ "./node_modules/rxjs/_esm5/internal/util/tryCatch.js");
/* harmony import */ var _util_errorObject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../util/errorObject */ "./node_modules/rxjs/_esm5/internal/util/errorObject.js");
/* harmony import */ var _Observable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../Observable */ "./node_modules/rxjs/_esm5/internal/Observable.js");
/* harmony import */ var _Subscriber__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../Subscriber */ "./node_modules/rxjs/_esm5/internal/Subscriber.js");
/* harmony import */ var _operators_map__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../operators/map */ "./node_modules/rxjs/_esm5/internal/operators/map.js");
/** PURE_IMPORTS_START tslib,_.._util_root,_.._util_tryCatch,_.._util_errorObject,_.._Observable,_.._Subscriber,_.._operators_map PURE_IMPORTS_END */







function getCORSRequest() {
    if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest) {
        return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest();
    }
    else if (!!_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XDomainRequest) {
        return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XDomainRequest();
    }
    else {
        throw new Error('CORS is not supported by your browser');
    }
}
function getXMLHttpRequest() {
    if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest) {
        return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest();
    }
    else {
        var progId = void 0;
        try {
            var progIds = ['Msxml2.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.4.0'];
            for (var i = 0; i < 3; i++) {
                try {
                    progId = progIds[i];
                    if (new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].ActiveXObject(progId)) {
                        break;
                    }
                }
                catch (e) {
                }
            }
            return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].ActiveXObject(progId);
        }
        catch (e) {
            throw new Error('XMLHttpRequest is not supported by your browser');
        }
    }
}
function ajaxGet(url, headers) {
    if (headers === void 0) {
        headers = null;
    }
    return new AjaxObservable({ method: 'GET', url: url, headers: headers });
}
function ajaxPost(url, body, headers) {
    return new AjaxObservable({ method: 'POST', url: url, body: body, headers: headers });
}
function ajaxDelete(url, headers) {
    return new AjaxObservable({ method: 'DELETE', url: url, headers: headers });
}
function ajaxPut(url, body, headers) {
    return new AjaxObservable({ method: 'PUT', url: url, body: body, headers: headers });
}
function ajaxPatch(url, body, headers) {
    return new AjaxObservable({ method: 'PATCH', url: url, body: body, headers: headers });
}
var mapResponse = /*@__PURE__*/ Object(_operators_map__WEBPACK_IMPORTED_MODULE_6__["map"])(function (x, index) { return x.response; });
function ajaxGetJSON(url, headers) {
    return mapResponse(new AjaxObservable({
        method: 'GET',
        url: url,
        responseType: 'json',
        headers: headers
    }));
}
var AjaxObservable = /*@__PURE__*/ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AjaxObservable, _super);
    function AjaxObservable(urlOrRequest) {
        var _this = _super.call(this) || this;
        var request = {
            async: true,
            createXHR: function () {
                return this.crossDomain ? getCORSRequest() : getXMLHttpRequest();
            },
            crossDomain: true,
            withCredentials: false,
            headers: {},
            method: 'GET',
            responseType: 'json',
            timeout: 0
        };
        if (typeof urlOrRequest === 'string') {
            request.url = urlOrRequest;
        }
        else {
            for (var prop in urlOrRequest) {
                if (urlOrRequest.hasOwnProperty(prop)) {
                    request[prop] = urlOrRequest[prop];
                }
            }
        }
        _this.request = request;
        return _this;
    }
    AjaxObservable.prototype._subscribe = function (subscriber) {
        return new AjaxSubscriber(subscriber, this.request);
    };
    AjaxObservable.create = (function () {
        var create = function (urlOrRequest) {
            return new AjaxObservable(urlOrRequest);
        };
        create.get = ajaxGet;
        create.post = ajaxPost;
        create.delete = ajaxDelete;
        create.put = ajaxPut;
        create.patch = ajaxPatch;
        create.getJSON = ajaxGetJSON;
        return create;
    })();
    return AjaxObservable;
}(_Observable__WEBPACK_IMPORTED_MODULE_4__["Observable"]));

var AjaxSubscriber = /*@__PURE__*/ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AjaxSubscriber, _super);
    function AjaxSubscriber(destination, request) {
        var _this = _super.call(this, destination) || this;
        _this.request = request;
        _this.done = false;
        var headers = request.headers = request.headers || {};
        if (!request.crossDomain && !headers['X-Requested-With']) {
            headers['X-Requested-With'] = 'XMLHttpRequest';
        }
        if (!('Content-Type' in headers) && !(_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData && request.body instanceof _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData) && typeof request.body !== 'undefined') {
            headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        }
        request.body = _this.serializeBody(request.body, request.headers['Content-Type']);
        _this.send();
        return _this;
    }
    AjaxSubscriber.prototype.next = function (e) {
        this.done = true;
        var _a = this, xhr = _a.xhr, request = _a.request, destination = _a.destination;
        var response = new AjaxResponse(e, xhr, request);
        if (response.response === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
            destination.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
        }
        else {
            destination.next(response);
        }
    };
    AjaxSubscriber.prototype.send = function () {
        var _a = this, request = _a.request, _b = _a.request, user = _b.user, method = _b.method, url = _b.url, async = _b.async, password = _b.password, headers = _b.headers, body = _b.body;
        var createXHR = request.createXHR;
        var xhr = Object(_util_tryCatch__WEBPACK_IMPORTED_MODULE_2__["tryCatch"])(createXHR).call(request);
        if (xhr === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
            this.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
        }
        else {
            this.xhr = xhr;
            this.setupEvents(xhr, request);
            var result = void 0;
            if (user) {
                result = Object(_util_tryCatch__WEBPACK_IMPORTED_MODULE_2__["tryCatch"])(xhr.open).call(xhr, method, url, async, user, password);
            }
            else {
                result = Object(_util_tryCatch__WEBPACK_IMPORTED_MODULE_2__["tryCatch"])(xhr.open).call(xhr, method, url, async);
            }
            if (result === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
                this.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
                return null;
            }
            if (async) {
                xhr.timeout = request.timeout;
                xhr.responseType = request.responseType;
            }
            if ('withCredentials' in xhr) {
                xhr.withCredentials = !!request.withCredentials;
            }
            this.setHeaders(xhr, headers);
            result = body ? Object(_util_tryCatch__WEBPACK_IMPORTED_MODULE_2__["tryCatch"])(xhr.send).call(xhr, body) : Object(_util_tryCatch__WEBPACK_IMPORTED_MODULE_2__["tryCatch"])(xhr.send).call(xhr);
            if (result === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
                this.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
                return null;
            }
        }
        return xhr;
    };
    AjaxSubscriber.prototype.serializeBody = function (body, contentType) {
        if (!body || typeof body === 'string') {
            return body;
        }
        else if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData && body instanceof _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData) {
            return body;
        }
        if (contentType) {
            var splitIndex = contentType.indexOf(';');
            if (splitIndex !== -1) {
                contentType = contentType.substring(0, splitIndex);
            }
        }
        switch (contentType) {
            case 'application/x-www-form-urlencoded':
                return Object.keys(body).map(function (key) { return encodeURIComponent(key) + "=" + encodeURIComponent(body[key]); }).join('&');
            case 'application/json':
                return JSON.stringify(body);
            default:
                return body;
        }
    };
    AjaxSubscriber.prototype.setHeaders = function (xhr, headers) {
        for (var key in headers) {
            if (headers.hasOwnProperty(key)) {
                xhr.setRequestHeader(key, headers[key]);
            }
        }
    };
    AjaxSubscriber.prototype.setupEvents = function (xhr, request) {
        var progressSubscriber = request.progressSubscriber;
        function xhrTimeout(e) {
            var _a = xhrTimeout, subscriber = _a.subscriber, progressSubscriber = _a.progressSubscriber, request = _a.request;
            if (progressSubscriber) {
                progressSubscriber.error(e);
            }
            var ajaxTimeoutError = new AjaxTimeoutError(this, request);
            if (ajaxTimeoutError.response === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
                subscriber.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
            }
            else {
                subscriber.error(ajaxTimeoutError);
            }
        }
        xhr.ontimeout = xhrTimeout;
        xhrTimeout.request = request;
        xhrTimeout.subscriber = this;
        xhrTimeout.progressSubscriber = progressSubscriber;
        if (xhr.upload && 'withCredentials' in xhr) {
            if (progressSubscriber) {
                var xhrProgress_1;
                xhrProgress_1 = function (e) {
                    var progressSubscriber = xhrProgress_1.progressSubscriber;
                    progressSubscriber.next(e);
                };
                if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XDomainRequest) {
                    xhr.onprogress = xhrProgress_1;
                }
                else {
                    xhr.upload.onprogress = xhrProgress_1;
                }
                xhrProgress_1.progressSubscriber = progressSubscriber;
            }
            var xhrError_1;
            xhrError_1 = function (e) {
                var _a = xhrError_1, progressSubscriber = _a.progressSubscriber, subscriber = _a.subscriber, request = _a.request;
                if (progressSubscriber) {
                    progressSubscriber.error(e);
                }
                var ajaxError = new AjaxError('ajax error', this, request);
                if (ajaxError.response === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
                    subscriber.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
                }
                else {
                    subscriber.error(ajaxError);
                }
            };
            xhr.onerror = xhrError_1;
            xhrError_1.request = request;
            xhrError_1.subscriber = this;
            xhrError_1.progressSubscriber = progressSubscriber;
        }
        function xhrReadyStateChange(e) {
            return;
        }
        xhr.onreadystatechange = xhrReadyStateChange;
        xhrReadyStateChange.subscriber = this;
        xhrReadyStateChange.progressSubscriber = progressSubscriber;
        xhrReadyStateChange.request = request;
        function xhrLoad(e) {
            var _a = xhrLoad, subscriber = _a.subscriber, progressSubscriber = _a.progressSubscriber, request = _a.request;
            if (this.readyState === 4) {
                var status_1 = this.status === 1223 ? 204 : this.status;
                var response = (this.responseType === 'text' ? (this.response || this.responseText) : this.response);
                if (status_1 === 0) {
                    status_1 = response ? 200 : 0;
                }
                if (status_1 < 400) {
                    if (progressSubscriber) {
                        progressSubscriber.complete();
                    }
                    subscriber.next(e);
                    subscriber.complete();
                }
                else {
                    if (progressSubscriber) {
                        progressSubscriber.error(e);
                    }
                    var ajaxError = new AjaxError('ajax error ' + status_1, this, request);
                    if (ajaxError.response === _util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"]) {
                        subscriber.error(_util_errorObject__WEBPACK_IMPORTED_MODULE_3__["errorObject"].e);
                    }
                    else {
                        subscriber.error(ajaxError);
                    }
                }
            }
        }
        xhr.onload = xhrLoad;
        xhrLoad.subscriber = this;
        xhrLoad.progressSubscriber = progressSubscriber;
        xhrLoad.request = request;
    };
    AjaxSubscriber.prototype.unsubscribe = function () {
        var _a = this, done = _a.done, xhr = _a.xhr;
        if (!done && xhr && xhr.readyState !== 4 && typeof xhr.abort === 'function') {
            xhr.abort();
        }
        _super.prototype.unsubscribe.call(this);
    };
    return AjaxSubscriber;
}(_Subscriber__WEBPACK_IMPORTED_MODULE_5__["Subscriber"]));

var AjaxResponse = /*@__PURE__*/ (function () {
    function AjaxResponse(originalEvent, xhr, request) {
        this.originalEvent = originalEvent;
        this.xhr = xhr;
        this.request = request;
        this.status = xhr.status;
        this.responseType = xhr.responseType || request.responseType;
        this.response = parseXhrResponse(this.responseType, xhr);
    }
    return AjaxResponse;
}());

function AjaxErrorImpl(message, xhr, request) {
    Error.call(this);
    this.message = message;
    this.name = 'AjaxError';
    this.xhr = xhr;
    this.request = request;
    this.status = xhr.status;
    this.responseType = xhr.responseType || request.responseType;
    this.response = parseXhrResponse(this.responseType, xhr);
    return this;
}
AjaxErrorImpl.prototype = /*@__PURE__*/ Object.create(Error.prototype);
var AjaxError = AjaxErrorImpl;
function parseJson(xhr) {
    if ('response' in xhr) {
        return xhr.responseType ? xhr.response : JSON.parse(xhr.response || xhr.responseText || 'null');
    }
    else {
        return JSON.parse(xhr.responseText || 'null');
    }
}
function parseXhrResponse(responseType, xhr) {
    switch (responseType) {
        case 'json':
            return Object(_util_tryCatch__WEBPACK_IMPORTED_MODULE_2__["tryCatch"])(parseJson)(xhr);
        case 'xml':
            return xhr.responseXML;
        case 'text':
        default:
            return ('response' in xhr) ? xhr.response : xhr.responseText;
    }
}
function AjaxTimeoutErrorImpl(xhr, request) {
    AjaxError.call(this, 'ajax timeout', xhr, request);
    this.name = 'AjaxTimeoutError';
    return this;
}
var AjaxTimeoutError = AjaxTimeoutErrorImpl;
//# sourceMappingURL=AjaxObservable.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/observable/dom/ajax.js":
/*!*****************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/observable/dom/ajax.js ***!
  \*****************************************************************/
/*! exports provided: ajax */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return ajax; });
/* harmony import */ var _AjaxObservable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AjaxObservable */ "./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js");
/** PURE_IMPORTS_START _AjaxObservable PURE_IMPORTS_END */

var ajax = _AjaxObservable__WEBPACK_IMPORTED_MODULE_0__["AjaxObservable"].create;
//# sourceMappingURL=ajax.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/util/root.js":
/*!*******************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/util/root.js ***!
  \*******************************************************/
/*! exports provided: root */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "root", function() { return _root; });
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var __window = typeof window !== 'undefined' && window;
var __self = typeof self !== 'undefined' && typeof WorkerGlobalScope !== 'undefined' &&
    self instanceof WorkerGlobalScope && self;
var __global = typeof global !== 'undefined' && global;
var _root = __window || __global || __self;
/*@__PURE__*/ (function () {
    if (!_root) {
        throw /*@__PURE__*/ new Error('RxJS could not find any global context (window, self, global)');
    }
})();

//# sourceMappingURL=root.js.map


/***/ }),

/***/ "./src/app/route-s2a.pipe.ts":
/*!***********************************!*\
  !*** ./src/app/route-s2a.pipe.ts ***!
  \***********************************/
/*! exports provided: RouteS2aPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteS2aPipe", function() { return RouteS2aPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RouteS2aPipe = /** @class */ (function () {
    function RouteS2aPipe() {
    }
    RouteS2aPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return args.concat(value.split('/'));
    };
    RouteS2aPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'routeS2a'
        })
    ], RouteS2aPipe);
    return RouteS2aPipe;
}());



/***/ }),

/***/ "./src/app/rxjs/basic/callbacks.component.ts":
/*!***************************************************!*\
  !*** ./src/app/rxjs/basic/callbacks.component.ts ***!
  \***************************************************/
/*! exports provided: CallbackComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallbackComponent", function() { return CallbackComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");



var CallbackComponent = /** @class */ (function () {
    function CallbackComponent() {
    }
    CallbackComponent.prototype.ngOnInit = function () {
        var _this = this;
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        var button = this.btn.nativeElement;
        var onClick = function (e) {
            log('click');
            ajax('/api/parse', function (err, data) {
                log(err, data);
                ajax('/api/create', function (error, record) {
                    log('record', record);
                });
            });
        };
        button.addEventListener('click', onClick);
        setTimeout(function () {
            button.removeEventListener('click', onClick);
        }, 2000);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], CallbackComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], CallbackComponent.prototype, "list", void 0);
    CallbackComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-promises',
            template: "\n    <h1>Callbacks</h1>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CallbackComponent);
    return CallbackComponent;
}());

function ajax(url, cb) {
    fetch(url)
        .then(function (res) { return res.json(); })
        .then(function (res) { return cb(null, res); }, function (err) { return cb(err); });
}
/**

    // callbacks
    const onClick = (e) => {
      ajax('/api/parse', (err, data) => {
        ajax('/api/create', (error, record) => {
          log('record', record);
        });
      });

      // button.removeEventListener('click', onClick);
    };
    button.addEventListener('click', onClick);

 */


/***/ }),

/***/ "./src/app/rxjs/basic/create.component.ts":
/*!************************************************!*\
  !*** ./src/app/rxjs/basic/create.component.ts ***!
  \************************************************/
/*! exports provided: CreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateComponent", function() { return CreateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var CreateComponent = /** @class */ (function () {
    function CreateComponent() {
    }
    CreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        var button = this.btn.nativeElement;
        // const stream$ = interval(1000);
        // const stream$ = of(1000);
        // const stream$ = empty();
        // const stream$ = EMPTY;
        // const stream$ = range(2, 4);
        // const stream$ = timer(2000);
        var stream$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('custom error');
        stream$.subscribe(function (val) { return log('next', val); }, function (err) { return log('error', err); }, function () { return log('complete'); });
        var btn$ = rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"].create(function (obs) {
            var counter = 0;
            function onClick(e) {
                if (counter > 3) {
                    return obs.error('nie nie nie ');
                }
                counter++;
                obs.next(e);
            }
            button.addEventListener('click', onClick);
            return function () {
                log('clean your resources');
                button.removeEventListener('click', onClick);
            };
        });
        // btn$.subscribe(
        //   (val) => log('next', val),
        //   (err) => log('error', err),
        //   () => log('complete')
        // );
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], CreateComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], CreateComponent.prototype, "list", void 0);
    CreateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-create',
            template: "\n    <h1>\n      Create Observables\n    </h1>\n    <p>\n    dost\u0119pne metody: Create, Defer, Empty/Never/Throw, From, Interval, Just, Range, Repeat, Start, and Timer\n    </p>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        })
    ], CreateComponent);
    return CreateComponent;
}());

/**

    const btn$ = Observable.create((obs) => {
      obs.next('custom value 1');
      // obs.error('custom error');
      // obs.complete();
      obs.next('custom value 2');
      const onClick = (e) => {
        obs.next(e);
      };
      button.addEventListener('click', onClick);

      return () => {
        log('clean your resources');
        button.removeEventListener('click', onClick);
      };

    });
 */


/***/ }),

/***/ "./src/app/rxjs/basic/intro.component.ts":
/*!***********************************************!*\
  !*** ./src/app/rxjs/basic/intro.component.ts ***!
  \***********************************************/
/*! exports provided: BasicIntroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicIntroComponent", function() { return BasicIntroComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");



var BasicIntroComponent = /** @class */ (function () {
    function BasicIntroComponent() {
    }
    BasicIntroComponent.prototype.ngOnInit = function () {
        var _this = this;
        var button = this.btn.nativeElement;
        console.log('btn', button);
        console.log('list', this.list);
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        log('my value label', 'my value to log');
        log('my value to log');
        log('my value label', 'my value to log');
        button.addEventListener('click', log);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], BasicIntroComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], BasicIntroComponent.prototype, "list", void 0);
    BasicIntroComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-intro',
            template: "\n    <h1>RxJS Playground</h1>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BasicIntroComponent);
    return BasicIntroComponent;
}());



/***/ }),

/***/ "./src/app/rxjs/basic/observable.component.ts":
/*!****************************************************!*\
  !*** ./src/app/rxjs/basic/observable.component.ts ***!
  \****************************************************/
/*! exports provided: ObservableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ObservableComponent", function() { return ObservableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");




var ObservableComponent = /** @class */ (function () {
    function ObservableComponent() {
    }
    ObservableComponent.prototype.ngOnInit = function () {
        var _this = this;
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        var button = this.btn.nativeElement;
        // const btn$: Observable<MouseEvent> = fromEvent(button, 'click');
        var btn$ = rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"].create(function (obs) {
            var counter = 0;
            function onClick(e) {
                if (counter > 3) {
                    return obs.complete('nie nie nie ');
                }
                counter++;
                obs.next(e);
            }
            button.addEventListener('click', onClick);
            return function () {
                log('clean your resources');
                button.removeEventListener('click', onClick);
            };
        });
        var observer = {
            next: function (val) { return _this.list.add('next', val); },
            error: function (err) { return _this.list.add('error', err); },
            complete: function () { return _this.list.add('complete'); },
        };
        var subscription = btn$.subscribe(observer);
        setTimeout(function () {
            log('timeout unsubscribe');
            subscription.unsubscribe();
        }, 3000);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ObservableComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_3__["ListComponent"])
    ], ObservableComponent.prototype, "list", void 0);
    ObservableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-observable',
            template: "\n    <h1>Observable</h1>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        })
    ], ObservableComponent);
    return ObservableComponent;
}());

/**

    // const btn$: Observable<MouseEvent> = fromEvent(button, 'click');

    const btn$ = Observable.create((obs) => {
      obs.next('custom value 1');
      // obs.error('custom error');
      // obs.complete();
      obs.next('custom value 2');

      button.addEventListener('click', (e) => {
        obs.next(e);
      });
    });

    const observer: Observer<MouseEvent> = {
      next: (val) => this.list.add('next', val),
      error: (err) => this.list.add('error', err),
      complete: () => this.list.add('complete'),
    };

    const subscription: Subscription = btn$.subscribe(observer);

    setTimeout(() => {
      log('unsubscribe');
      subscription.unsubscribe();
    }, 5000);

    // const sbuscription2 = btn$.subscribe(
    //   (val) => this.list.add('next', val),
    //   (err) => this.list.add('error', err),
    //   () => this.list.add('complete')
    // );

 */


/***/ }),

/***/ "./src/app/rxjs/basic/pipe.component.ts":
/*!**********************************************!*\
  !*** ./src/app/rxjs/basic/pipe.component.ts ***!
  \**********************************************/
/*! exports provided: PipeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PipeComponent", function() { return PipeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var PipeComponent = /** @class */ (function () {
    function PipeComponent() {
    }
    PipeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        var button = this.btn.nativeElement;
        var input = this.input.nativeElement;
        var btn$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(button, 'click');
        var input$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(input, 'keyup'); // TODO e.target.value
        var interval$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["interval"])(1000);
        function myOperator(in$) {
            // return in$.pipe(
            // );
            var subj$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
            in$.subscribe(function (v) {
                subj$.next(v + 1);
            });
            return subj$.asObservable();
        }
        var keyboard$ = input$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (e) { return e.target.value; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])(), 
        // filter(v => v.length > 2),
        // debounce(() => interval$),
        // debounceTime(250),
        // bufferTime(2000)
        myOperator, Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["buffer"])(interval$));
        keyboard$.subscribe(function (v) { return log('V', v); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('input'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], PipeComponent.prototype, "input", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], PipeComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], PipeComponent.prototype, "list", void 0);
    PipeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pipe',
            template: "\n    <h1>\n      Metoda pipe() na Observable\n    </h1>\n    <h2>operatory</h2>\n    <p>\n      podstawowe: map, filter, reduce\n    </p>\n    <input #input type=\"text\" id=\"textInput\" class=\"form-control\" placeholder=\"Enter Query...\" autocomplete=\"false\">\n    <pre>{{text}}</pre>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        })
    ], PipeComponent);
    return PipeComponent;
}());



/***/ }),

/***/ "./src/app/rxjs/basic/promises.component.ts":
/*!**************************************************!*\
  !*** ./src/app/rxjs/basic/promises.component.ts ***!
  \**************************************************/
/*! exports provided: PromisesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromisesComponent", function() { return PromisesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");



var PromisesComponent = /** @class */ (function () {
    function PromisesComponent() {
    }
    PromisesComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var log, button, myTask, data, data2, error_1, onClick;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        log = function () {
                            var args = [];
                            for (var _i = 0; _i < arguments.length; _i++) {
                                args[_i] = arguments[_i];
                            }
                            var _a;
                            return (_a = _this.list).add.apply(_a, args);
                        };
                        button = this.btn.nativeElement;
                        myTask = new Promise(function (resolve, reject) {
                            setTimeout(function () {
                                resolve({ title: 'my data' });
                            }, 2000);
                        });
                        myTask.then(function (data) { return log(data); });
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, fetch('/api/parse')
                                .then()
                                .catch(function (err) { return Promise.resolve({ title: 'my data from cache' }); })];
                    case 2:
                        data = _a.sent();
                        return [4 /*yield*/, fetch('/api/parse')];
                    case 3:
                        data2 = _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log('error', error_1);
                        return [3 /*break*/, 5];
                    case 5:
                        onClick = function (e) {
                            log(e);
                            fetch('/api/parse')
                                .then(function (res) { return res.json(); })
                                .then(function (data) {
                                if (!data) {
                                    return Promise.reject('no data  from server');
                                }
                                return fetch('/api/create', { method: 'GET' }).then(function (res) { return res.json(); });
                            })
                                .catch(function (err) {
                                return Promise.resolve({ title: 'my data from cache' });
                            })
                                .then(function (data) {
                                return fetch('/api/parse');
                            })
                                .catch(function (err) { return log('err', err); });
                            // button.removeEventListener('click', onClick);
                        };
                        button.addEventListener('click', onClick);
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], PromisesComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], PromisesComponent.prototype, "list", void 0);
    PromisesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-promises',
            template: "\n    <h1>Promise</h1>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PromisesComponent);
    return PromisesComponent;
}());



/***/ }),

/***/ "./src/app/rxjs/basic/subject.component.ts":
/*!*************************************************!*\
  !*** ./src/app/rxjs/basic/subject.component.ts ***!
  \*************************************************/
/*! exports provided: SubjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubjectComponent", function() { return SubjectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var SubjectComponent = /** @class */ (function () {
    function SubjectComponent() {
        // @OnDestroy();
        this.destroy$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.showList = true;
    }
    SubjectComponent.prototype.ngOnInit = function () {
        var _this = this;
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        var button = this.btn.nativeElement;
        // const subject$ = new Subject();
        // const subject$ = new BehaviorSubject(-1);
        var subject$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["ReplaySubject"](2);
        var counter = 0;
        button.addEventListener('click', function () {
            counter++;
            log('counter', counter);
            subject$.next(counter);
        });
        this.list$ = subject$.asObservable().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (value) { return log('tap', value); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.destroy$.asObservable()));
        // this.list$.subscribe(val => this.lista = val);
    };
    SubjectComponent.prototype.ngOnDestroy = function () {
        // NIE
        // if(this.subscription) {
        //   this.subscription.unsubscribe();
        // }
        // TAK
        this.destroy$.next();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], SubjectComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], SubjectComponent.prototype, "list", void 0);
    SubjectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-subject',
            template: "\n    <h1>\n      Subjects\n    </h1>\n    <p>\n      Subject\n      BehaviorSubject\n      ReplaySubject\n    </p>\n    <button #btn class=\"btn btn-primary\">Button</button>\n\n    <hr>\n    <button (click)=\"showList = !showList\" class=\"btn btn-primary\">toggle debug</button>\n    <p>\n    <b *ngIf=\"showList\"> {{list$ | async | json }} </b>\n    </p>\n\n    <hr>\n    <app-list #list></app-list>\n  "
        })
    ], SubjectComponent);
    return SubjectComponent;
}());



/***/ }),

/***/ "./src/app/rxjs/example/autocomplete.component.ts":
/*!********************************************************!*\
  !*** ./src/app/rxjs/example/autocomplete.component.ts ***!
  \********************************************************/
/*! exports provided: AutocompleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutocompleteComponent", function() { return AutocompleteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_ajax__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/ajax */ "./node_modules/rxjs/_esm5/ajax/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent() {
        this.items = [];
    }
    AutocompleteComponent.prototype.ngOnInit = function () {
        var _this = this;
        function searchWikipedia(term) {
            return rxjs_ajax__WEBPACK_IMPORTED_MODULE_3__["ajax"].getJSON('/api/wikipedia?limit=5&search=' + term).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) { return response; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) { return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])([{ title: 'error: ' + err.message }]); }));
        }
        var input = this.input.nativeElement;
        var keyup$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(input, 'keyup').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (e) { return e.target.value; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function (text) { return text.length > 2; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(250), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (text) { return searchWikipedia(text); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (results) { return results.filter(function (k, i) { return i < 5; }); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["share"])());
        keyup$.subscribe(function (data) {
            console.log('data A', data);
            _this.items = data;
        });
        // keyup$.subscribe((data: any) => {
        //   console.log('data B', data);
        //   this.items = data;
        // });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('input'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], AutocompleteComponent.prototype, "input", void 0);
    AutocompleteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-autocomplete',
            template: "\n    <h1>Autocomplete</h1>\n    <form role=\"form\">\n      <div class=\"form-group\">\n        <label for=\"textInput\">Enter Query for Wikipedia</label>\n        <input #input type=\"text\" id=\"textInput\" class=\"form-control\" placeholder=\"Enter Query...\">\n      </div>\n    </form>\n\n    <h2>Wyniki <small>({{items.length}})</small></h2>\n    <ul class=\"list-group\">\n      <li class=\"list-group-item\" *ngFor=\"let item of items; let i=index\">\n        {{item.title}}\n      </li>\n    </ul>\n\n  "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AutocompleteComponent);
    return AutocompleteComponent;
}());

/**
 *
    function searchWikipedia(term) {
      return ajax.getJSON('/api/wikipedia?search=' + term).pipe(
        map(response => response),
        catchError(err => {
          return of([{title: 'error: ' + err.message}]);
        })
      );
    }

    const input = this.input.nativeElement;

    const keyup$ = fromEvent(input, 'keyup').pipe(
      map((e: any) => e.target.value),
      filter((text) =>  text.length > 2),
      distinctUntilChanged(),
      debounceTime(250),
      switchMap(text => searchWikipedia(text))
    );

    keyup$.subscribe((data: any) => {
      console.log('data', data);
      this.items = data;
    });

 */


/***/ }),

/***/ "./src/app/rxjs/example/drag-and-drop.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/rxjs/example/drag-and-drop.component.ts ***!
  \*********************************************************/
/*! exports provided: DragAndDropComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DragAndDropComponent", function() { return DragAndDropComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var DragAndDropComponent = /** @class */ (function () {
    function DragAndDropComponent() {
    }
    DragAndDropComponent.prototype.ngOnInit = function () {
        var box = this.box.nativeElement;
        var host = this.host.nativeElement;
        var explode$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        var down$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(box, 'mousedown');
        var move$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(document, 'mousemove');
        var up$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(document, 'mouseup');
        var drag$ = down$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (downEvent) {
            // miejsce kliknięcia na stronie
            var startX = downEvent.pageX;
            var startY = downEvent.pageY;
            // obecna pozycja boxa
            var boxX = parseInt(box.style.left, 10) || 0;
            var boxY = parseInt(box.style.top, 10) || 0;
            // host
            var hostX = parseInt(host.style.width, 10) || 0;
            var hostY = parseInt(host.style.height, 10) || 0;
            return move$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (moveEvent) {
                var pos = {
                    x: boxX + moveEvent.pageX - startX,
                    y: boxY + moveEvent.pageY - startY
                };
                if (pos.x < hostX) {
                    explode$.next(pos);
                    pos.x = hostX;
                }
                if (pos.y < hostY) {
                    explode$.next(pos);
                    pos.y = hostY;
                }
                return pos;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(up$), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(explode$));
        }));
        drag$.subscribe(function (pos) {
            box.style.top = pos.y + 'px';
            box.style.left = pos.x + 'px';
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('box'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], DragAndDropComponent.prototype, "box", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('host'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], DragAndDropComponent.prototype, "host", void 0);
    DragAndDropComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-drag-and-drop',
            template: "\n    <h1>Drag & Drop</h1>\n    <div #host class=\"host\">\n      <div #box></div>\n    </div>\n  ",
            styles: ["\n    .host {height: 200px; width: 100%; border: 1px solid gray; position: relative;}\n    .host > div {background-color: black; width: 50px; height: 50px; position: absolute; top: 10; left: 10;}\n  "]
        })
    ], DragAndDropComponent);
    return DragAndDropComponent;
}());

/**

    const box = this.box.nativeElement;

    const move$ = fromEvent(document, 'mousemove');
    const down$ = fromEvent(document, 'mousedown');
    const up$ = fromEvent(document, 'mouseup');

    const drag$ = down$.pipe(
      mergeMap((downEvent) => {
        return move$.pipe(takeUntil(up$));
      })
    );

    drag$.subscribe((pos) => console.log(pos)));

 */
/**

    const box = this.box.nativeElement;

    const down$ = fromEvent<MouseEvent>(box, 'mousedown');
    const move$ = fromEvent<MouseEvent>(document, 'mousemove');
    const up$ = fromEvent<MouseEvent>(document, 'mouseup');

    const drag$ = down$.pipe(
      mergeMap((downEvent) => {

        // miejsce kliknięcia na stronie
        const startX = downEvent.pageX;
        const startY = downEvent.pageY;

        // obecna pozycja boxa
        const boxX = parseInt(box.style.left, 10) || 0;
        const boxY = parseInt(box.style.top, 10) || 0;

        return move$.pipe(
          map( function( moveEvent ) {
            return {
              x: boxX + moveEvent.pageX - startX,
              y: boxY + moveEvent.pageY - startY
            };
          }),
          takeUntil(up$)
        );
      })
    );

    drag$.subscribe((pos) => {
      box.style.top = pos.y + 'px';
      box.style.left = pos.x + 'px';
    });

 */


/***/ }),

/***/ "./src/app/rxjs/example/ngrx.component.ts":
/*!************************************************!*\
  !*** ./src/app/rxjs/example/ngrx.component.ts ***!
  \************************************************/
/*! exports provided: NgrxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgrxComponent", function() { return NgrxComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");



var NgrxComponent = /** @class */ (function () {
    function NgrxComponent(store) {
        this.items = [];
    }
    NgrxComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('input'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NgrxComponent.prototype, "input", void 0);
    NgrxComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ngrx',
            template: "\n    <h1>NgRx</h1>\n    <form role=\"form\">\n      <div class=\"form-group\">\n        <label for=\"textInput\">Enter Query for Wikipedia</label>\n        <input #input type=\"text\" id=\"textInput\" class=\"form-control\" placeholder=\"Enter Query...\">\n      </div>\n    </form>\n\n    <h2>Wyniki <small>({{items.length}})</small></h2>\n    <ul class=\"list-group\">\n      <li class=\"list-group-item\" *ngFor=\"let item of items; let i=index\">\n        {{item.title}}\n      </li>\n    </ul>\n\n  "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], NgrxComponent);
    return NgrxComponent;
}());

/**
 *
    function searchWikipedia(term) {
      return ajax.getJSON('/api/wikipedia?search=' + term).pipe(
        map(response => response),
        catchError(err => {
          return of([{title: 'error: ' + err.message}]);
        })
      );
    }

    const input = this.input.nativeElement;

    const keyup$ = fromEvent(input, 'keyup').pipe(
      map((e: any) => e.target.value),
      filter((text) =>  text.length > 2),
      distinctUntilChanged(),
      debounceTime(250),
      switchMap(text => searchWikipedia(text))
    );

    keyup$.subscribe((data: any) => {
      console.log('data', data);
      this.items = data;
    });

 */


/***/ }),

/***/ "./src/app/rxjs/operators/operators.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/rxjs/operators/operators.component.ts ***!
  \*******************************************************/
/*! exports provided: OperatorsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OperatorsComponent", function() { return OperatorsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/list/list.component */ "./src/app/shared/list/list.component.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var OperatorsComponent = /** @class */ (function () {
    function OperatorsComponent() {
    }
    OperatorsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var log = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var _a;
            return (_a = _this.list).add.apply(_a, args);
        };
        var button = this.btn.nativeElement;
        var input = this.input.nativeElement;
        var btn$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(button, 'click');
        var input$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["fromEvent"])(input, 'keyup');
        var config$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]('config');
        // const stream$ = combineLatest(
        //   config$,
        //   btn$.pipe(startWith(1)),
        //   input$.pipe(
        //     map((e: any) => e.target.value),
        //     startWith('start')
        //     )
        // ).pipe(
        //   map(([c, b, i]) => {
        //     return '';
        //   })
        // );
        // stream$.subscribe(v => log(v));
        var interval$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["interval"])(1000);
        var sharedInterval$ = interval$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["share"])());
        sharedInterval$.subscribe(function (v) { return log('A', v); });
        setTimeout(function () {
            sharedInterval$.subscribe(function (v) { return log('B', v); });
        }, 4000);
        setTimeout(function () {
            interval$.subscribe(function (v) { return log('Z', v); });
        }, 5000);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('input'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], OperatorsComponent.prototype, "input", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], OperatorsComponent.prototype, "btn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('list'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_shared_list_list_component__WEBPACK_IMPORTED_MODULE_2__["ListComponent"])
    ], OperatorsComponent.prototype, "list", void 0);
    OperatorsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-operators',
            template: "\n  <h1>\n  Operatory\n    </h1>\n    <p>\n      zaawansowane: switchMap, debounceTime throttleTime combineLatest retry merge delay bufferTime switchMap takeUntil\n    </p>\n    <input #input type=\"text\" id=\"textInput\" class=\"form-control\" placeholder=\"Enter Query...\" autocomplete=\"false\">\n    <pre>{{text}}</pre>\n    <button #btn class=\"btn btn-primary\">Button</button>\n    <app-list #list></app-list>\n  "
        })
    ], OperatorsComponent);
    return OperatorsComponent;
}());



/***/ }),

/***/ "./src/app/rxjs/routes.ts":
/*!********************************!*\
  !*** ./src/app/rxjs/routes.ts ***!
  \********************************/
/*! exports provided: ROUTES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony import */ var _basic_callbacks_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic/callbacks.component */ "./src/app/rxjs/basic/callbacks.component.ts");
/* harmony import */ var _basic_promises_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/promises.component */ "./src/app/rxjs/basic/promises.component.ts");
/* harmony import */ var _basic_observable_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic/observable.component */ "./src/app/rxjs/basic/observable.component.ts");
/* harmony import */ var _basic_create_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./basic/create.component */ "./src/app/rxjs/basic/create.component.ts");
/* harmony import */ var _basic_pipe_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./basic/pipe.component */ "./src/app/rxjs/basic/pipe.component.ts");
/* harmony import */ var _operators_operators_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./operators/operators.component */ "./src/app/rxjs/operators/operators.component.ts");
/* harmony import */ var _basic_subject_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./basic/subject.component */ "./src/app/rxjs/basic/subject.component.ts");
/* harmony import */ var _example_drag_and_drop_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./example/drag-and-drop.component */ "./src/app/rxjs/example/drag-and-drop.component.ts");
/* harmony import */ var _example_autocomplete_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./example/autocomplete.component */ "./src/app/rxjs/example/autocomplete.component.ts");
/* harmony import */ var _example_ngrx_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./example/ngrx.component */ "./src/app/rxjs/example/ngrx.component.ts");
/* harmony import */ var _basic_intro_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./basic/intro.component */ "./src/app/rxjs/basic/intro.component.ts");











var ROUTES = [{
        path: '',
        redirectTo: 'observable',
        pathMatch: 'full'
    }, {
        path: 'intro',
        component: _basic_intro_component__WEBPACK_IMPORTED_MODULE_10__["BasicIntroComponent"]
    }, {
        path: 'callbacks',
        component: _basic_callbacks_component__WEBPACK_IMPORTED_MODULE_0__["CallbackComponent"]
    }, {
        path: 'promises',
        component: _basic_promises_component__WEBPACK_IMPORTED_MODULE_1__["PromisesComponent"]
    }, {
        path: 'observable',
        component: _basic_observable_component__WEBPACK_IMPORTED_MODULE_2__["ObservableComponent"]
    }, {
        path: 'create',
        component: _basic_create_component__WEBPACK_IMPORTED_MODULE_3__["CreateComponent"]
    }, {
        path: 'pipe',
        component: _basic_pipe_component__WEBPACK_IMPORTED_MODULE_4__["PipeComponent"]
    }, {
        path: 'operators',
        component: _operators_operators_component__WEBPACK_IMPORTED_MODULE_5__["OperatorsComponent"]
    }, {
        path: 'subject',
        component: _basic_subject_component__WEBPACK_IMPORTED_MODULE_6__["SubjectComponent"]
    }, {
        path: 'drag-and-drop',
        component: _example_drag_and_drop_component__WEBPACK_IMPORTED_MODULE_7__["DragAndDropComponent"]
    }, {
        path: 'autocomplete',
        component: _example_autocomplete_component__WEBPACK_IMPORTED_MODULE_8__["AutocompleteComponent"]
    }, {
        path: 'ngrx',
        component: _example_ngrx_component__WEBPACK_IMPORTED_MODULE_9__["NgrxComponent"]
    }];


/***/ }),

/***/ "./src/app/rxjs/rxjs-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/rxjs/rxjs-routing.module.ts ***!
  \*********************************************/
/*! exports provided: RxjsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxjsRoutingModule", function() { return RxjsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./routes */ "./src/app/rxjs/routes.ts");
/* harmony import */ var _rxjs_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./rxjs.component */ "./src/app/rxjs/rxjs.component.ts");





var routes = [{
        path: '',
        component: _rxjs_component__WEBPACK_IMPORTED_MODULE_4__["RxjsComponent"],
        children: _routes__WEBPACK_IMPORTED_MODULE_3__["ROUTES"]
    }];
var RxjsRoutingModule = /** @class */ (function () {
    function RxjsRoutingModule() {
    }
    RxjsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], RxjsRoutingModule);
    return RxjsRoutingModule;
}());



/***/ }),

/***/ "./src/app/rxjs/rxjs.component.css":
/*!*****************************************!*\
  !*** ./src/app/rxjs/rxjs.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3J4anMvcnhqcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/rxjs/rxjs.component.html":
/*!******************************************!*\
  !*** ./src/app/rxjs/rxjs.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\">\n  <a class=\"navbar-brand col-sm-3 col-md-2 mr-0\" routerLink=\"/\">RxJS</a>\n  <ul class=\"navbar-nav px-3\">\n    <li class=\"nav-item text-nowrap\">\n      <!-- <a class=\"nav-link\" href=\"#\">Sign out</a> -->\n    </li>\n  </ul>\n</nav>\n\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\">\n      <div class=\"sidebar-sticky\">\n\n        <!-- <h6 class=\"sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted\">\n          <span>Streams</span>\n          <a class=\"d-flex align-items-center text-muted\" href=\"#\">\n            <span data-feather=\"plus-circle\"></span>\n          </a>\n        </h6> -->\n\n        <ul class=\"nav flex-column\">\n          <li class=\"nav-item\" *ngFor=\"let route of routes; let i=index\">\n            <a class=\"nav-link\" [routerLink]=\"route.path | routeS2a:'/rxjs'\" routerLinkActive=\"active\">\n              <span data-feather=\"file-text\"></span>\n              {{route.path}}\n            </a>\n          </li>\n        </ul>\n\n      </div>\n    </nav>\n\n    <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4\">\n      <router-outlet></router-outlet>\n    </main>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/rxjs/rxjs.component.ts":
/*!****************************************!*\
  !*** ./src/app/rxjs/rxjs.component.ts ***!
  \****************************************/
/*! exports provided: RxjsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxjsComponent", function() { return RxjsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./routes */ "./src/app/rxjs/routes.ts");



var RxjsComponent = /** @class */ (function () {
    function RxjsComponent() {
        this.routes = _routes__WEBPACK_IMPORTED_MODULE_2__["ROUTES"];
    }
    RxjsComponent.prototype.ngOnInit = function () {
    };
    RxjsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rxjs',
            template: __webpack_require__(/*! ./rxjs.component.html */ "./src/app/rxjs/rxjs.component.html"),
            styles: [__webpack_require__(/*! ./rxjs.component.css */ "./src/app/rxjs/rxjs.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RxjsComponent);
    return RxjsComponent;
}());



/***/ }),

/***/ "./src/app/rxjs/rxjs.module.ts":
/*!*************************************!*\
  !*** ./src/app/rxjs/rxjs.module.ts ***!
  \*************************************/
/*! exports provided: RxjsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxjsModule", function() { return RxjsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _rxjs_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rxjs-routing.module */ "./src/app/rxjs/rxjs-routing.module.ts");
/* harmony import */ var _rxjs_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./rxjs.component */ "./src/app/rxjs/rxjs.component.ts");
/* harmony import */ var _basic_callbacks_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./basic/callbacks.component */ "./src/app/rxjs/basic/callbacks.component.ts");
/* harmony import */ var _basic_promises_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./basic/promises.component */ "./src/app/rxjs/basic/promises.component.ts");
/* harmony import */ var _basic_intro_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./basic/intro.component */ "./src/app/rxjs/basic/intro.component.ts");
/* harmony import */ var _operators_operators_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./operators/operators.component */ "./src/app/rxjs/operators/operators.component.ts");
/* harmony import */ var _route_s2a_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../route-s2a.pipe */ "./src/app/route-s2a.pipe.ts");
/* harmony import */ var _basic_observable_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./basic/observable.component */ "./src/app/rxjs/basic/observable.component.ts");
/* harmony import */ var _example_drag_and_drop_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./example/drag-and-drop.component */ "./src/app/rxjs/example/drag-and-drop.component.ts");
/* harmony import */ var _shared_list_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../shared/list/list.component */ "./src/app/shared/list/list.component.ts");
/* harmony import */ var _example_autocomplete_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./example/autocomplete.component */ "./src/app/rxjs/example/autocomplete.component.ts");
/* harmony import */ var _basic_pipe_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./basic/pipe.component */ "./src/app/rxjs/basic/pipe.component.ts");
/* harmony import */ var _basic_create_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./basic/create.component */ "./src/app/rxjs/basic/create.component.ts");
/* harmony import */ var _basic_subject_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./basic/subject.component */ "./src/app/rxjs/basic/subject.component.ts");
/* harmony import */ var _example_ngrx_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./example/ngrx.component */ "./src/app/rxjs/example/ngrx.component.ts");


















var RxjsModule = /** @class */ (function () {
    function RxjsModule() {
    }
    RxjsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _rxjs_component__WEBPACK_IMPORTED_MODULE_4__["RxjsComponent"],
                _basic_callbacks_component__WEBPACK_IMPORTED_MODULE_5__["CallbackComponent"],
                _basic_promises_component__WEBPACK_IMPORTED_MODULE_6__["PromisesComponent"],
                _basic_intro_component__WEBPACK_IMPORTED_MODULE_7__["BasicIntroComponent"],
                _operators_operators_component__WEBPACK_IMPORTED_MODULE_8__["OperatorsComponent"],
                _route_s2a_pipe__WEBPACK_IMPORTED_MODULE_9__["RouteS2aPipe"],
                _basic_observable_component__WEBPACK_IMPORTED_MODULE_10__["ObservableComponent"],
                _example_drag_and_drop_component__WEBPACK_IMPORTED_MODULE_11__["DragAndDropComponent"],
                _shared_list_list_component__WEBPACK_IMPORTED_MODULE_12__["ListComponent"],
                _example_autocomplete_component__WEBPACK_IMPORTED_MODULE_13__["AutocompleteComponent"],
                _basic_pipe_component__WEBPACK_IMPORTED_MODULE_14__["PipeComponent"],
                _basic_create_component__WEBPACK_IMPORTED_MODULE_15__["CreateComponent"],
                _basic_subject_component__WEBPACK_IMPORTED_MODULE_16__["SubjectComponent"],
                _example_ngrx_component__WEBPACK_IMPORTED_MODULE_17__["NgrxComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _rxjs_routing_module__WEBPACK_IMPORTED_MODULE_3__["RxjsRoutingModule"]
            ]
        })
    ], RxjsModule);
    return RxjsModule;
}());



/***/ }),

/***/ "./src/app/shared/list/list.component.css":
/*!************************************************!*\
  !*** ./src/app/shared/list/list.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9saXN0L2xpc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/list/list.component.html":
/*!*************************************************!*\
  !*** ./src/app/shared/list/list.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>List <small>({{items.length}})</small></h2>\n<ul class=\"list-group\">\n  <li class=\"list-group-item\" *ngFor=\"let item of items; let i=index\">\n    {{items.length-i}} - <b> {{item.label}}</b> {{item.item}}\n  </li>\n</ul>\n"

/***/ }),

/***/ "./src/app/shared/list/list.component.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/list/list.component.ts ***!
  \***********************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ListComponent = /** @class */ (function () {
    function ListComponent() {
        var _this = this;
        this.items = [];
        this.add = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            console.log.apply(console, [_this.items.length + 1].concat(args));
            var item;
            if (args.length === 1) {
                item = {
                    label: '',
                    item: args[0]
                };
            }
            else {
                item = {
                    label: args[0],
                    item: args[1]
                };
            }
            _this.items.unshift(item);
        };
    }
    ListComponent.prototype.ngOnInit = function () {
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/shared/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/shared/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ListComponent);
    return ListComponent;
}());



/***/ })

}]);
//# sourceMappingURL=rxjs-rxjs-module.js.map