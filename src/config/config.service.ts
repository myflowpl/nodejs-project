import { Injectable } from '@nestjs/common';
import { resolve } from 'path';

@Injectable()
export class ConfigService {
  _JWT_SECRET = 'jwt-secret';

  TOKEN_HEADER_NAME = 'api-token';

  get JWT_SECRET_REFACTOR() {
    return process.env.JWT_SECRET || this._JWT_SECRET;
  }

  STORAGE_TMP = resolve(__dirname, '../../storage/tmp');
  STORAGE_PHOTOS = resolve(__dirname, '../../storage/photos');
  PHOTOS_BASE_PATH = '/assets/photos';

  DB_NAME = resolve(__dirname, '../../storage/databases/db.sql');

  constructor() {
    // npm install dotenv
  }
}
