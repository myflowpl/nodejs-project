#!/usr/bin/env node

const inquirer = require('inquirer');
const chalk = require('chalk');
const path = require('path');
// const {throwError} = require('./throw-error')

const askForEnv = async function (env) {


  let message = 'environement forced to'
  if(!env) {
    const inputs = await inquirer.prompt([{
      type: 'list',
      name: 'env',
      default: 'dev',
      message: 'select environment',
      choices: ['dev', 'test', 'prod']
    }]);
    env = inputs.env;
    message = 'environement selected to:';
  }

  // const BASE_DIR = process.env[`SM_${env.toUpperCase()}_DIR`];
  const BASE_DIR = './';
  // if(!BASE_DIR) {
  //   throwError(`process.env.SM_${env.toUpperCase()}_DIR - this env variable has to be defined`)
  // }

  console.log(chalk.green('?'), message, chalk.blue(env), 'on', BASE_DIR);

  return {
    ENV: env,
    BASE_DIR
  };
}

const {NestFactory} = require('@nestjs/core')
// const {askForEnv} = require('./utils/ask-for-env')
const {resolve} = require('path')



async function stats() {
  const env = await askForEnv();
  console.log('TODO: Display stats for env:', env)
  // const moduleAlias = require("module-alias");
  // moduleAlias.addPath(resolve(__dirname, '../dist'));

  const {AppModule} = require('../dist/app.module')
  const {AppService} = require('../dist/app.service')
  const app = await NestFactory.create(AppModule);
  const appService = app.get(AppService);
  console.log('STATS', appService.getHello())
}
stats();
